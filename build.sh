
TOOLS=~/.local/android_tools/cmdline-tools/latest/build-tools/28.0.1

rm -rf ./deploy
mkdir -p ./deploy

zig build-lib \
    -dynamic \
    -target aarch64-linux-android \
    -I/home/prototano/.local/android-ndk-r23b/toolchains/llvm/prebuilt/linux-x86_64/sysroot/usr/include/aarch64-linux-android/\
    -lc -llog -landroid -lEGL -lGLESv1_CM --libc libc.txt \
    main.c


mkdir -p ./deploy/lib/arm64-v8a
mv libmain.so ./deploy/lib/arm64-v8a/

cp -r res ./deploy/res

java -jar xml2axml.jar e AndroidManifest.xml deploy/AndroidManifest.xml

# Enter in the deploy folder
cd deploy

# Seems that create an empty file classes.dex is enough
touch classes.dex

zip -r myapp.zip *
mv myapp.zip myapp.apk
$TOOLS/zipalign -c 4 myapp.apk
echo -e "123456\n12345678" | $TOOLS/apksigner sign --ks ../mykey.keystore  myapp.apk
$TOOLS/apksigner verify  myapp.apk

